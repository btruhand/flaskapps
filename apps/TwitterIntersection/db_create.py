from migrate.versioning import api
from twitterintersect import config
from twitterintersect import db
import os.path

db.create_all()
SQLALCHEMY_DATABASE_URI = config.DefaultConfig.SQLALCHEMY_DATABASE_URI
SQLALCHEMY_MIGRATE_REPO = config.DefaultConfig.SQLALCHEMY_MIGRATE_REPO

if not os.path.exists(SQLALCHEMY_MIGRATE_REPO):
	api.create(SQLALCHEMY_MIGRATE_REPO, 'db_repository')
	api.version_control(SQLALCHEMY_DATABASE_URI, SQLALCHEMY_MIGRATE_REPO)
else:
	api.version_control(SQLALCHEMY_DATABASE_URI, SQLALCHEMY_MIGRATE_REPO, api.version(SQLALCHEMY_MIGRATE_REPO))
