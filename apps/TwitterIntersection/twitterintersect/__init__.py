from celery import Celery
from flask import Flask
from flask.ext.sqlalchemy import SQLAlchemy

app = Flask(__name__)
app.config.from_object('twitterintersect.config.DefaultConfig')
db = SQLAlchemy(app)

from twitterintersect import views
