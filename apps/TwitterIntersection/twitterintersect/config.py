import os

basedir = os.path.abspath(os.path.dirname(__file__))

class DefaultConfig(object):
	SECRET_KEY = '\x9f\xc8\x0bT\x85\x8a\xf4\xd4\x9f\x800\x05\xf6\xcb@h\x1e\x92\x94\x1b\xa5b\xac&xH\xf4\x93\x8f'
	SQLALCHEMY_DATABASE_URI = 'sqlite:///' + os.path.join(basedir, "app.db")
	SQLALCHEMY_MIGRATE_REPO = os.path.join(basedir, "db_repository")	
	CELERY_BROKER_URL = 'amqp://guest@localhost:5672//'
	CELERY_RESULT_BACKEND = 'amqp://'
	CELERYD_PREFETCH_MULTIPLIER = 1
#	use_x_sendfile = True
