from twitterintersect import db

SEARCH_NO = 0
SEARCH_YES = 1
SEARCH_ERROR = 2
NO_SEARCH = ''
NO_ERROR = ''

class Searched(db.Model):
	id = db.Column(db.Integer, primary_key = True, autoincrement = True)
	file_name = db.Column(db.String(100), index = True, unique = True)
	searched_by = db.Column(db.String(100))
	date_searched = db.Column(db.String(15))

class TwitterUser(db.Model):
	id = db.Column(db.Integer, primary_key = True, autoincrement = True)
	screen_name = db.Column(db.String(100), index = True, unique = True)
	search_status = db.Column(db.Integer)
	searching = db.Column(db.String(100), index = True, unique = True)
	error = db.Column(db.String(100))
