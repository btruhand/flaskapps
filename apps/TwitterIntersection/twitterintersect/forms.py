from flask.ext.wtf import Form
from wtforms import TextField
from wtforms.validators import InputRequired, ValidationError
from flask import session
from twitterintersect.db_models import Searched


class SearchForm(Form):
	search = TextField('search', validators=[InputRequired()])
