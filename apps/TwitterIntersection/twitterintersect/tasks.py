from twitterintersect import app, db
from twitterintersect.hackedtwitter import Api
from twitterintersect.appInfo import CONSUMER_KEY, CONSUMER_SECRET
from twitterintersect.db_models import *
from celery import Celery
from time import strftime
import os

def make_celery(application):
    celery = Celery(application.import_name, broker=application.config['CELERY_BROKER_URL'])
    celery.conf.update(application.config)
    TaskBase = celery.Task
    class ContextTask(TaskBase):
        abstract = True
        def __call__(self, *args, **kwargs):
            with application.app_context():
                return TaskBase.__call__(self, *args, **kwargs)
    celery.Task = ContextTask
    return celery

celery = make_celery(app)

@celery.task
def search(search, access_key_secret, searcher):
	theApi = Api(consumer_key = CONSUMER_KEY,
				consumer_secret = CONSUMER_SECRET,
				access_token_key = access_key_secret[0],
				access_token_secret = access_key_secret[1])

	try:
		follower_IDs = theApi.GetFollowerIDs(screen_name=search, stringify_ids = True)
	except Exception, e:
		twitter_user = TwitterUser.query.filter_by(screen_name = searcher).first()
		twitter_user.search_status = SEARCH_ERROR
		twitter_user.error = str(e)
		db.session.commit()
		return None 

	with open(os.path.dirname(os.path.realpath(__file__))+'/static/search_result/'+search+'.txt', 'w') as rewriteFile:
		for ids in follower_IDs:
			rewriteFile.write(ids + '\n')
	twitter_user = TwitterUser.query.filter_by(screen_name = searcher).first()
	twitter_user.search_status = SEARCH_NO
	
	search_before = Searched.query.filter_by(file_name = search).first()
	if search_before:
		search_before.searched_by = searcher
		search_before.date_search = strftime("%d/%m/%Y")
	else:
		searched = Searched(file_name = search, searched_by = searcher, date_searched = strftime("%d/%m/%Y"))
		db.session.add(searched)
	db.session.commit()
