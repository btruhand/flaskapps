from twitterintersect import app, db
from twitterintersect.hackedtwitter import Api
from twitterintersect.appInfo import CONSUMER_KEY, CONSUMER_SECRET
from twitterintersect import tasks
from twitterintersect.db_models import *
from flask import render_template, session, url_for, redirect, request, flash, g, send_file, send_from_directory, make_response
from flask_oauth import OAuth
from forms import SearchForm
import os
import uuid

path_URL = ''
oauth = OAuth()

twitter = oauth.remote_app('followers', base_url = 'https://api.twitter.com/',
		request_token_url = 'https://api.twitter.com/oauth/request_token',
		access_token_url = 'https://api.twitter.com/oauth/access_token',
		authorize_url = 'https://api.twitter.com/oauth/authorize',
		consumer_key = CONSUMER_KEY,
		consumer_secret = CONSUMER_SECRET)


def allinSet(check_set, with_set):
	for user in check_set:
		if not user in with_set:
			return False
	return True

def netIntersection(subset_in_question, bigger_subsets, file_name, write):
	subsets_with_usernames = [subset for subset in bigger_subsets if allinSet(subset_in_question, subset)]
	first = True
	users_followers = None
	for username in subset_in_question:
		with open(os.path.dirname(os.path.realpath(__file__))+'/static/search_result/'+username+'.txt') as f:
			if first:
				users_followers = set(f.readlines())
				first = False
			else:
				users_followers.intersection_update(f.readlines())

	for subset in subsets_with_usernames:
		inner_first = True
		take_out_followers = None
		for username in subset:
			with open(os.path.dirname(os.path.realpath(__file__))+'/static/search_result/'+username+'.txt') as f:
				if inner_first:
					take_out_followers = set(f.readlines())
					inner_first = False
				else:
					take_out_followers.intersection_update(f.readlines())
		users_followers.difference_update(take_out_followers)

	result = "Users following only "
	first = True
	for user in subset_in_question:
		if first:
			result+= user+" "
			first = False
		else:
			result+= "and " + user

	result+= ":\n"

	first = True
	newline_counter = 1
	for follower in users_followers:
		if first:
			result+= follower.rstrip()
			first = False
		else:
			if newline_counter != 14:
				result+= ','+follower.rstrip()
				newline_counter+=1
			else:
				result+= '\n'+follower.rstrip()
				newline_counter = 0

	result+= "Total in this intersection: " + str(len(users_followers)) + '\n'
	g.reach+= len(users_followers)
	if write:
		with open(file_name, 'w') as f:
			f.write(result)
	else:
		with open(file_name, 'a') as f:
			f.write(result)

def process_intersection(list_of_names, file_name):
	from itertools import chain, combinations
	non_empty_subsets = list(chain.from_iterable(combinations(list_of_names, size) for size in range(1,len(list_of_names)+1)))
        reach = 0
	first = True
        for subset in non_empty_subsets:
                if len(subset) < len(list_of_names):
                        bigger_subsets = list(combinations(list_of_names, len(subset)+1))
                        if first:
				netIntersection(subset, bigger_subsets, file_name, True)
				first = False
			else:
				netIntersection(subset, bigger_subsets, file_name, False)
                else:
                        netIntersection(subset, [], file_name, False)

        with open(file_name, 'a') as f:
		f.write("Total reach is: " + str(g.reach) + "\n")

@app.route(path_URL+'/')
def first_page():
	return render_template('index.html')

@app.route(path_URL+'/logout/')
def logout():
	del session['user_token']
	del session['screen_name']
	return redirect(url_for('first_page'))

@app.route(path_URL+'/intersect/', methods=['GET', 'POST'])
def intersection():
	if request.method == 'GET':
		return render_template('intersect.html')
	elif request.method == 'POST' and 'number' in request.form:
		try:
			amount = int(request.form['number'])
		except:
			flash('Invalid input. Input needs to be a whole number')
			return render_template('intersect.html')
		files = [{'file_name' : searched.file_name, 'last_searched' : searched.date_searched} for searched in Searched.query]
		return render_template('intersect.html', amount = amount, files = files)

@app.route(path_URL+'/intersect/result', methods=['GET','POST'])
def run_intersection():
	if not request.form.has_key('files'):
		flash('No Twitter user was picked for their follower intersections')
		return redirect(url_for('first_page'))
	file_list = set(request.form.getlist('files'))
	directory = os.path.dirname(os.path.realpath(__file__))+'/static/intersect_result/'
	file_name = str(uuid.uuid4())
	g.reach = 0
	process_intersection(file_list, directory+file_name)
	download_file = send_file(directory+file_name, mimetype='text/plain', as_attachment=True, attachment_filename='result.txt')
	return make_response(download_file)

@app.route(path_URL+'/find-followers/')
def ask_for_login():
	if session.has_key('screen_name'):
		return redirect(url_for('search_follower'))	
	return render_template('login.html')

@app.route(path_URL+'/find-followers/login')
def redirect_oauth():
	if session.has_key('user_token'):
		return redirect(url_for('search_follower'))
	return twitter.authorize(callback = url_for('oauth_authorized'))

@twitter.tokengetter
def get_user_twitter_token():
	return session.get('user_token')
		
@app.route(path_URL+'/oauth_authorized/')
@twitter.authorized_handler
def oauth_authorized(response):
	if response is None:
		flash('You denied login access using your Twitter account')
		return redirect(url_for('first_page'))

	oauth_token = response['oauth_token']
	oauth_token_secret = response['oauth_token_secret']
	session['user_token'] = (oauth_token, oauth_token_secret)
	session['screen_name'] = response['screen_name']
	check_user = TwitterUser.query.filter_by(screen_name = response['screen_name']).first()
	if not check_user:
		twitter_user = TwitterUser(screen_name = response['screen_name'], search_status = SEARCH_NO, searching = NO_SEARCH, error = NO_ERROR)
		db.session.add(twitter_user)
		db.session.commit()
	return redirect(url_for('search_follower'))

@app.route(path_URL+'/search/', methods=['GET','POST'])
def search_follower(search=None):
	if not session.has_key('user_token') or not session['screen_name']:
		flash('You cannot access that feature without logging in through Twitter')
		return redirect(url_for('first_page'))	
	check_user = TwitterUser.query.filter_by(screen_name = session['screen_name']).first()
	if check_user:
		if check_user.search_status == SEARCH_YES:
			flash('Cannot access that functionality right now your account is in a search process')
			return redirect(url_for('first_page'))

	if search:
		tasks.search.delay(search, get_user_twitter_token(), session['screen_name'])
		flash('Your search is in progress')
		return redirect(url_for('first_page'))

	message = "Please enter the Twitter screen name you want to search"	

	form = SearchForm()
	if form.validate_on_submit():
		name_to_search = form.search.data
		access_key_secret = get_user_twitter_token()
		api = Api(consumer_key = CONSUMER_KEY,
				consumer_secret = CONSUMER_SECRET,
				access_token_key = access_key_secret[0],
				access_token_secret = access_key_secret[1])
		
		if api.UsersLookup(screen_name = [name_to_search]) == []:
			flash('Given an invalid Twitter screen name')
			return render_template('search.html', form=form, message=message)
		else:
			already = Searched.query.filter_by(file_name = name_to_search).first()
			if already:
				flash('The Twitter account: {account} has already been searched on {date} by the Twitter account {searcher}'.format(account=already.file_name, date=already.date_searched, searcher=already.searched_by))
				return render_template('affirm.html', to_search = name_to_search)
			
			return redirect(url_for('run_search', search = name_to_search))
			
	return render_template('search.html', form=form, message=message)

@app.route(path_URL+'/run-search/<search>')
def run_search(search = None):
	if not request.referrer:
		flash('You cannot access that webpage manually')
		return redirect(url_for('first_page'))

	if not session.has_key('screen_name'):
		flash('You cannot access that feature without logging in through Twitter')
		return redirect(url_for('first_page'))	
	current_user = TwitterUser.query.filter_by(screen_name = session['screen_name']).first()
	current_user.searching = search
	current_user.search_status = SEARCH_YES
	current_user.error = NO_ERROR
	db.session.commit()
	tasks.search.delay(search, get_user_twitter_token(), session['screen_name'])
	flash('Your search is in progress')
	return redirect(url_for('first_page'))

@app.route(path_URL+'/check-status/', methods=['GET', 'POST'])
def check_status():
	message = "Please enter the Twitter name that made the search"
	form = SearchForm()
	if form.validate_on_submit():
		name_to_search = form.search.data
		query_result = TwitterUser.query.filter_by(screen_name = name_to_search).first()
		if query_result:
			if query_result.search_status == SEARCH_ERROR:
				result_resp = "An error occurred for your search: " + search_query.error
			
			if query_result.search_status == SEARCH_YES:
				result_resp = "The search for followers of " + query_result.searching + " is still ongoing"
			else:
				if query_result.searching == NO_SEARCH:
					result_resp = "The user has not initialized any new searches"
				else:
					result_resp = "The search for followers of " + query_result.searching + " succeeded and has finished"
			
			return render_template('search.html', form=form, message=message, result_resp=result_resp)	
		else:
			flash('Twitter user never used this application')
			message = "Please enter the Twitter name that made the search"
			return render_template('search.html', form=form, message=message)
	
	return render_template('search.html', form=form, message=message)

@app.route(path_URL+'/testing/')
def test():
	return send_file('/home/btruhand/Desktop/test.html', mimetype='text/html', as_attachment = True, attachment_filename = 'test.html')
